using System;
using System.Collections.Generic;
using System.Linq;

namespace TransPerfectAPI.models{
    
    public class QRCodeModel{

        public QRCodeModel(){

        }

        public QRCodeModel(string _type, SymbolModel[] _symbolModels){
            this.type = _type;
            this.symbol = _symbolModels;
        }

        public string type {get;set;}
        public SymbolModel[] symbol{get;set;}

    }

}