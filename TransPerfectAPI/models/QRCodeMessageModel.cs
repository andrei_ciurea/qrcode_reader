namespace TransPerfectAPI.models{
    
    public class QRCodeMessageModel{

        public QRCodeMessageModel(){

        }

        public QRCodeMessageModel(string _message){
            this.message = _message;
        }

        public string message {get;set;}

    }

}