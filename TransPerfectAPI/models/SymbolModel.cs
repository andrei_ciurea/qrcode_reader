using System;
using System.Collections.Generic;
using System.Linq;

namespace TransPerfectAPI.models{
    
    public class SymbolModel{

        public SymbolModel(){

        }

        public SymbolModel(string _seq, string _data, string _error){
            this.seq = _seq;
            this.data = _data;
            this.error = _error;
        }

        public string seq {get;set;}
        public string data {get;set;}
        public string error {get;set;}

    }

}