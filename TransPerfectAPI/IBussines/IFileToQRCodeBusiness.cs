public interface IFileToQRCodeBusiness
{
    byte[] getDataBytes(string filePath);   
}