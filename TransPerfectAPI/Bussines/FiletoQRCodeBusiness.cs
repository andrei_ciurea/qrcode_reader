using System.IO;

namespace TransPerfectAPI.Controllers
{
    public class FiletoQRCodeBusiness : IFileToQRCodeBusiness { 

        public byte[] getDataBytes(string filePath){
            
            byte[] data = File.ReadAllBytes(filePath);

            return data;
        }
    }
}