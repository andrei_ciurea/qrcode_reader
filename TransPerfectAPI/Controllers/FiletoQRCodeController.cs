using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TransPerfectAPI.models;
using Newtonsoft.Json;
using System.Text.Json;

namespace TransPerfectAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class FileToQRCodeController : ControllerBase
    {

        private readonly ILogger<FileToQRCodeController> _logger;
        private readonly IFileToQRCodeBusiness _fileToQRCodeBusiness;
        public FileToQRCodeController(ILogger<FileToQRCodeController> logger,IFileToQRCodeBusiness fileToQRCodeBusiness)
        {
            _logger = logger;
            _fileToQRCodeBusiness = fileToQRCodeBusiness;
        }

        [HttpGet]
        public async Task<ActionResult<string>> GetQRCode([FromBody] FilePathModel filePath){
            QRCodeModel qRCode = new QRCodeModel();
            
            HttpContent bytesContent = new ByteArrayContent(_fileToQRCodeBusiness.getDataBytes(filePath.filePath));

            using(var httpClient = new HttpClient()){
                using (var formData = new MultipartFormDataContent()){
                    
                    formData.Add(bytesContent, "file", filePath.filePath.Split('/').LastOrDefault());

                    using(var response = await httpClient.PostAsync("http://api.qrserver.com/v1/read-qr-code/",formData)){
                        if(response.IsSuccessStatusCode){
                            string apiResponse = await response.Content.ReadAsStringAsync();
                            qRCode = JsonConvert.DeserializeObject<List<QRCodeModel>>(apiResponse).FirstOrDefault();

                            return JsonConvert.SerializeObject(new QRCodeMessageModel(qRCode.symbol.FirstOrDefault().data));
                        }
                        
                    }
                }
            }
            return NotFound();
        }
    }
}